package ch.bwe.fab.v1.agent;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import ch.bwe.faa.v1.core.event.Event;
import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.fab.v1.type.Agent;

/**
 * @author benjamin
 *
 */
public class AgentStation {

	private List<Agent> agents = new LinkedList<>();

	/**
	 * Constructor.
	 */
	public AgentStation() {
		super();
	}

	/**
	 * Registers an agent to this station.
	 * 
	 * @param inAgent
	 *          the agent
	 */
	public void registerAgent(Agent inAgent) {
		AssertUtils.notNull(inAgent);

		synchronized (agents) {
			agents.add(inAgent);
		}
	}

	/**
	 * Unregisters an agent from this station.
	 * 
	 * @param inAgent
	 *          the agent
	 */
	public void unregisterAgent(Agent inAgent) {
		AssertUtils.notNull(inAgent);

		synchronized (agents) {
			agents.remove(inAgent);
		}
	}

	/**
	 * Shuts down all agents and unregisters them.
	 */
	public void shutdownAll() {
		synchronized (agents) {
			agents.forEach(a -> {
				a.stop();
				a.waitUntilStopped();
			});
			agents.clear();
		}
	}

	/**
	 * Gets all registered agents.
	 * 
	 * @return the agents
	 */
	public List<Agent> getRegisteredAgents() {
		synchronized (agents) {
			return Collections.unmodifiableList(agents);
		}
	}
	
	public void alertAll(final Event inEvent) {
		synchronized (agents) {
			agents.forEach(a -> a.onEvent(inEvent));
		}
	}
}
