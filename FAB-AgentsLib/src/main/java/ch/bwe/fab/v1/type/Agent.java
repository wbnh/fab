package ch.bwe.fab.v1.type;

import ch.bwe.faa.v1.core.command.CommandExecutor;
import ch.bwe.faa.v1.core.event.EventDispatcher;
import ch.bwe.faa.v1.core.event.EventListener;
import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fab.v1.agent.AgentStation;
import ch.bwe.fab.v1.util.AgentUtils;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author benjamin
 */
public abstract class Agent extends EventDispatcher implements Cloneable, Runnable, EventListener, CommandExecutor {

    /**
     * The time to wait in idle iterations.
     */
    private static final long IDLE_WAIT_TIME = 1000L;

    /**
     * Whether the agent is currently running.
     */
    private Boolean running = false;

    /**
     * Whether the agent has completed its mission.
     */
    private Boolean completed = false;

    /**
     * The agent's thread.
     */
    private Thread executionThread;

    /**
     * The agent's name.
     */
    private final String name;

    /**
     * The loops to do.
     */
    private int loopsToDo;

    /**
     * Whether to run infinitely.
     */
    private final boolean infiniteRun;

    /**
     * The amount of time in ms to wait between each loop.
     */
    private final long loopWaitTime;

    /**
     * The station to report to.
     */
    private final AgentStation station;

    /**
     * Constructor.
     *
     * @param inName The agent's name.
     */
    public Agent(String inName) {
        this(new AgentConfiguration(inName));
    }

    /**
     * Constructor.
     *
     * @param inConfiguration The configuration for the agent
     */
    public Agent(AgentConfiguration inConfiguration) {
        super();

        name = inConfiguration.name;
        loopsToDo = inConfiguration.loopsToDo;
        infiniteRun = loopsToDo < 0;
        loopWaitTime = inConfiguration.loopWait;
        station = inConfiguration.station;
    }

    /**
     * Starts the agent.
     */
    public final void start() {
        synchronized (running) {
            if (running) {
                ServiceRegistry.getLogProxy().trace(this, "Agent {0}: already started, cannot be started again", name);
                return;
            } // if running

            ServiceRegistry.getLogProxy().debug(this, "Agent {0}: starting", name);

            ensureNotCompleted();
            running = true;

            executionThread = new Thread(this);
            executionThread.start();
        } // synchronized running
    }

    /**
     * Asks the agent to pause; it can take a while before it stops. It can be
     * resumed.
     */
    public final void pause() {
        synchronized (running) {
            ServiceRegistry.getLogProxy().debug(this, "Agent {0}: pausing", name);
            running = false;
        } // synchronized running
    }

    /**
     * Asks the agent to pause; it can take a while before it stops. It will be
     * resumed after the passed limit has been reached. Counting starts
     * immediately.
     *
     * @param inTimeMillis the limit when the agent shall wake again.
     */
    public final void pause(long inTimeMillis) {
        Timer aTimer = new Timer();
        pause();
        aTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                resume();
            }
        }, inTimeMillis);
    }

    /**
     * Lets the agent resume.
     */
    public final void resume() {
        ensureNotCompleted();
        synchronized (running) {
            ServiceRegistry.getLogProxy().debug(this, "Agent {0}: resuming", name);
            running = true;
        } // synchronized running
    }

    /**
     * Asks the agent to stop; it can take a while before it stops. Once an agent
     * is stopped, it cannot be started again.
     */
    public final void stop() {
        synchronized (running) {
            synchronized (completed) {
                ServiceRegistry.getLogProxy().debug(this, "Agent {0}: stopping", name);
                running = false;
                completed = true;
            } // synchronized completed
        } // synchronized running
    }

    /**
     * Returns whether the agent is currently running.
     *
     * @return <code>true</code> if it is.
     */
    public final boolean isRunning() {
        return running;
    }

    /**
     * Returns whether the agent has completed its mission.
     *
     * @return <code>true</code> if it has, <code>false</code> otherwise
     */
    public final boolean isCompleted() {
        return completed;
    }

    /**
     * Ensures that the agent has not been completed yet.
     */
    private void ensureNotCompleted() {
        if (completed) {
            throw new IllegalStateException("The agent has already been stopped. It must not be started again.");
        } // if completed
    }

    /**
     * Checks whether this agent can be executed.
     *
     * @return <code>true</code> if it can, <code>false</code> otherwise
     */
    public boolean canExecute() {
        boolean outCanExecute = true;

        outCanExecute &= !isCompleted();
        outCanExecute &= executionThread == null;

        return outCanExecute;
    }

    public void run() {
        try {
            setup();

            for (; loopsToDo > 0 || infiniteRun; loopsToDo--) {
                if (!running) {
                    if (completed) {
                        ServiceRegistry.getLogProxy().trace(this, "Agent {0}: completed, stepping out of main loop", name);
                        break;
                    } // if completed

                    ServiceRegistry.getLogProxy().trace(this, "Agent {0}: paused, waiting {1}ms", name, IDLE_WAIT_TIME);
                    try {
                        Thread.sleep(IDLE_WAIT_TIME);
                    } catch (InterruptedException problem) {
                        ServiceRegistry.getLogProxy().trace(this, "Agent {0}: Thread interrupted; shutting down", name);
                        break;
                    } // catch
                    if (!infiniteRun) {
                        loopsToDo++;
                    }
                    continue;
                } // if not running

                loop();

                if (loopWaitTime > 0) {
                    try {
                        Thread.sleep(loopWaitTime);
                    } catch (InterruptedException problem) {
                        ServiceRegistry.getLogProxy().trace(this, "Agent {0}: Thread interrupted; shutting down", name);
                        break;
                    } // catch
                } // if has loop wait time
            }
        } catch (ThreadDeath death) {
            ServiceRegistry.getLogProxy().error(this,
                    "A ThreadDeath occurred. Please shut agents down correctly.\nShutting down normally...", death);
        } // catch

        teardown();
    }

    private void setup() {
        ServiceRegistry.getLogProxy().debug(this, "Agent {0}: setting up", name);

        if (station != null) {
            station.registerAgent(this);
        } // if station not null

        // ensure we do not lose the agent
        AgentUtils.registerAgentGlobal(this);

        doSetup();
        ServiceRegistry.getLogProxy().trace(this, "Agent {0}: set up", name);
    }

    /**
     * Sets the agent up.
     */
    protected abstract void doSetup();

    private void loop() {
        ServiceRegistry.getLogProxy().debug(this, "Agent {0}: performing loop iteration", name);
        doLoop();
        ServiceRegistry.getLogProxy().trace(this, "Agent {0}: performed loop iteration", name);
    }

    /**
     * Performs a loop iteration.
     */
    protected abstract void doLoop();

    private void teardown() {
        ServiceRegistry.getLogProxy().debug(this, "Agent {0}: shutting down", name);

        if (station != null) {
            station.unregisterAgent(this);
        } // if station not null

        // make sure we do not leak memory
        AgentUtils.unregisterAgentGlobal(this);

        doTeardown();
        ServiceRegistry.getLogProxy().trace(this, "Agent {0}: shut down", name);
    }

    /**
     * Tears the agent down.
     */
    protected abstract void doTeardown();

    /**
     * Waits until the agent is stopped.
     */
    public final void waitUntilStopped() {
        try {
            if (executionThread != null && executionThread.isAlive()) {
                executionThread.join();
            } // if has living execution thread
        } catch (InterruptedException problem) {
            Thread.currentThread().interrupt();
        } // catch
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Agent clone() throws CloneNotSupportedException {
        Agent outClone = (Agent) super.clone();
        outClone.running = false;
        outClone.completed = false;
        return outClone;
    }

    public String getName() {
        return name;
    }

    protected int getLoopsToDo() {
        return loopsToDo;
    }
}
