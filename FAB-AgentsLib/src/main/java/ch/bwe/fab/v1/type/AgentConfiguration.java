package ch.bwe.fab.v1.type;

import ch.bwe.faa.v1.core.util.AssertUtils;
import ch.bwe.fab.v1.agent.AgentStation;

/**
 * @author benjamin
 *
 */
public class AgentConfiguration {

	String name;
	int loopsToDo = -1;
	long loopWait = -1L;
	AgentStation station = null;

	/**
	 * Constructor.
	 */
	public AgentConfiguration(String inName) {
		super();

		AssertUtils.notNull(inName);

		name = inName;
	}

	public AgentConfiguration loops(int inLoops) {
		loopsToDo = inLoops;
		return this;
	}

	public AgentConfiguration loopWait(long inMillis) {
		loopWait = inMillis;
		return this;
	}

	public AgentConfiguration station(AgentStation inStation) {
		station = inStation;
		return this;
	}
}
