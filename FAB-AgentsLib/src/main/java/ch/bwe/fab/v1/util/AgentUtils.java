package ch.bwe.fab.v1.util;

import ch.bwe.fab.v1.agent.AgentStation;
import ch.bwe.fab.v1.type.Agent;

/**
 * @author benjamin
 *
 */
public final class AgentUtils {

	/**
	 * The global agent station to handle all agents in case something goes really
	 * wrong. Is intended to be able to shut down all agents.
	 */
	private static AgentStation _globalStation = new AgentStation();

	/**
	 * Constructor.
	 */
	private AgentUtils() {
	}

	/**
	 * Registers the passed agent globally. Is intended to be able to shut down
	 * all agents.
	 * 
	 * @param inAgent
	 *          the agent to register
	 */
	public static void registerAgentGlobal(Agent inAgent) {
		_globalStation.registerAgent(inAgent);
	}

	/**
	 * Unregisters the passed agent globally. Is intended to be able to shut down
	 * all agents.
	 * 
	 * @param inAgent
	 *          the agent to unregister
	 */
	public static void unregisterAgentGlobal(Agent inAgent) {
		_globalStation.unregisterAgent(inAgent);
	}

	/**
	 * Shuts down all agents. Is intended for use in exceptional cases.
	 */
	public static void shutdownAllAgents() {
		_globalStation.shutdownAll();
	}
}
