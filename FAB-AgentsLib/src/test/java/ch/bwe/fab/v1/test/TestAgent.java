package ch.bwe.fab.v1.test;

import ch.bwe.faa.v1.core.event.Event;
import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.fab.v1.type.Agent;
import ch.bwe.fab.v1.type.AgentConfiguration;

/**
 * @author benjamin
 */
public class TestAgent extends Agent {

    public boolean setup = false;
    public int loops = 0;
    public boolean teardown = false;

    /**
     * Constructor.
     */
    public TestAgent() {
        super(new AgentConfiguration("TestAgent").loops(20));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doSetup() {
        ServiceRegistry.getLogProxy().info(this, "Setup");
        setup = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doLoop() {
        loops++;
        ServiceRegistry.getLogProxy().info(this, "Loop " + loops);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doTeardown() {
        ServiceRegistry.getLogProxy().info(this, "Teardown");
        teardown = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onEvent(Event inEvent) {
    }

}
