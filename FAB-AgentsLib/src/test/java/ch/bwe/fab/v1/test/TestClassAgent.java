package ch.bwe.fab.v1.test;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author benjamin
 *
 */
public class TestClassAgent {

	@Test
	public void testAgent() throws Exception {
		TestAgent aTestAgent = new TestAgent();
		aTestAgent.start();
		aTestAgent.waitUntilStopped();
		Assert.assertTrue(aTestAgent.setup);
		Assert.assertEquals(20, aTestAgent.loops);
		Assert.assertTrue(aTestAgent.teardown);
	}
}
